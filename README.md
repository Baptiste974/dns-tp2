Top Level Domain

options {
	directory "/etc/named";
};

zone "." {
	type hint;
	file "named.root";
};

zone "org" {
	type master;
	file "org";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};



$TTL 60000
@ IN SOA dorg.org. root.dorg.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)

@		IN  NS    dorg.org.
dorg.org.	IN  A     10.0.2.10

wiki.org.	IN  NS    dwikiorg.wiki.org.
dwikiorg.wiki.org.	IN  A     10.0.7.10





RootServer

options {
	directory "/etc/named";
};

zone "." {
	type master;
	file "root";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};

zone "IN-ADDR.ARPA" {
 	type master;
 	file "in-addr.arpa";
};





$TTL   60000
@    IN      SOA     aRootServer. root.aRootServer (
         2002102801 ; serial
         28800 ; refresh
         14400 ; retry
         3600000 ; expire
         0 ; default_ttl
)

@               IN   NS    aRootServer.
aRootServer.    IN   A	   10.0.0.10

org.            IN   NS    dorg.org.
dorg.org.       IN   A     10.0.2.10

re.             IN   NS    dre.re.
dre.re.         IN   A     10.0.3.10





$TTL	86400
@	IN	SOA	localhost. root.localhost (
	20041128 ; Serial
	28800	 ; Refresh
	7200	 ; Retry
	3600000	 ; Expire
	86400 	 ; Minimum
)
	IN	NS	localhost.
1 	IN 	PTR 	localhost.





$TTL   60000
@               IN      SOA     aRootServer. root.aRootServer (
         2002102801 ; serial
         28800 ; refresh
         14400 ; retry
         3600000 ; expire
         0 ; default_ttl
)

@			IN	NS	aRootServer.
10.0.0.10.in-addr.arpa.	IN	PTR	aRootServer.
10.3.0.10.in-addr.arpa.	IN	PTR	dre.re.
10.2.0.10.in-addr.arpa.	IN	PTR	dorg.org.
